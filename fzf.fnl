#!/usr/bin/env fennel
(local utils (require :lambda.utils))
(local tcat table.concat)

(local fzf
	   {:presets {}
		:feeders {}
		:borders
		{:rounded		"rounded"
		 :sharp			"sharp"
		 :horizontal	"horizontal"
		 :vertical		"vertical"
		 :top			"top"
		 :bottom		"bottom"
		 :left			"left"
		 :right			"right"
		 :none			"none"
		 }})

(fn fzf.capture [cmd]
  (local f (assert (io.popen cmd :r)))
  (var s (assert (f:read :*a)))
  (f:close)
  (local raw s)
  (set s (string.gsub s "^%s+" ""))
  (set s (string.gsub s "%s+$" ""))
  (set s (string.gsub s "[\n\r]+" " "))
  {:out s :raw raw})

(local tbl {})
(fn tbl.combine* [tbla tblb]
  (local combo tbla)
  (each [key value tblb]
	(tset combo key value)
	)
  combo)

(lambda tbl.combine [ta tb]
  (collect [key value (pairs tb) &into ta]
	(values key value)) ta)
(fn fzf.feeders.exec [input] input)


(fn fzf.feeders.echo [input]
  (.. "echo \"" (tcat input "\n")  "\"" ))

(fn fzf.feeders.cat [input]
  (.. "cat " input " " ))

(fn fzf.show [feeder input props]
  (fn argsBuilder
	[args]
	(local listen	(if args.listen		(.. "--listen " (or args.listen.port "6464")) ""))
	(local prompt	(if args.prompt		(.. "--prompt '"	args.prompt		"'")	""))
	(local pointer	(if args.pointer	(.. "--pointer '"	args.pointer	"'")	""))
	(local marker	(if args.marker		(.. "--marker '"	args.marker		"'")	""))
	(local hist		(if args.hist		(.. "--history='"	args.hist		"'")	""))
	(local bind		(if args.bind		(.. "--bind '"		args.bind		"'")	""))
	(local height	(if args.height		(.. "--height="		args.height		"%")	""))
	(local border	(if args.border		(.. "--border="		args.border			)	""))
	(local preview	(if args.preview	(.. "--preview '" (table.concat args.preview " ") "'") ""))
	(local nth
		   (if args.nth
			   (..
				(if args.nth.delimiter (.. "--delimiter='" (table.concat args.nth.delimiter ",") "' ") "")
				(if args.nth.delim (.. "--delimiter='" (table.concat args.nth.delim ",") "' ") "")
				(if args.nth.scope (.. "--nth=" (table.concat args.nth.scope ",") " ") " ")
				(if args.nth.display (.. "--with-nth=" (table.concat args.nth.display ",") " ") "")
				) ""))
	;;(local delim	(if args.delim "--delimiter" ""))
	(local cycle	(if args.cycle		"--cycle" ""))
	(local dumb		(if args.dumb		"--disabled +s" ""))
	(local multi	(if args.multi		"-m" ""))
	(local reverse	(if args.reverse	"--tac" ""))
	;;(local	(if args.instant "-n " " "))
	;; (local	 (if args.separator (.. "-d " args.separator) " "))
	(table.concat
	 [prompt pointer
	  marker multi
	  cycle height
	  dumb border
	  hist nth
	  bind preview] " "))
  
  ;;(.. "echo \"" (table.concat input "\n") "\"|fzf " (argsBuilder props))
  (.. (feeder input) "|"  "fzf " (argsBuilder props))
  )
(fn fzf.show* [{: feeder : input : props}] (fzf.show feeder input props))

(local presets {})
(fn presets.selector [args]
  (collect [k v (pairs args) &into {:cycle true :dumb true}] (values k v)))

(fn presets.runNth [n args]
  "this one takes multiple arguments"
  (collect [k v (pairs args) &into
			{:bind (.. "enter:execute({" (table.concat n.scope " ") "})")
			 :nth n }] (values k v)))

(fn presets.becomeNth [n args]
  "this one takes multiple arguments"
  (collect [k v (pairs args) &into
			{:bind (.. "enter:become({" (table.concat n.scope " ") "})")
			 :nth n }] (values k v)))

;;(fn presets.runOutput [args] (collect [k v (pairs args) &into {}]))
(tset fzf :presets presets)

fzf
