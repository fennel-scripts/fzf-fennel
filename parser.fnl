#!/usr/bin/env fennel
(local tcat table.concat)
(local list
	   [{:sort "h" :name "htop" :cmd "htop"}
		{:sort "v" :name "volume" :cmd "ncpamixer"}
		{:sort "p" :name "package manager" :cmd "parui"}
		])
		 	
(each [k v (pairs list)]
	(print (tcat [k v.sort  v.name  v.cmd] "|")))

