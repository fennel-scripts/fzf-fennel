#!/usr/bin/env fennel

{:botsversion :3.5.1
 :name "fzf"
 :version :3.0.0
 :author "Erik Lundstedt"
 :license :SimPL2.0}
