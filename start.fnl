#!/usr/bin/env fennel
;; fzf
;; --delimiter="\|"
;; --with-nth 1,2
;; --nth "1,2"
;; --preview 'echo {1},{2},{3}'
;; --bind='enter:become({3})'
;; --preview-window=down,2
(local fzf (require :fzf))


(local settings
		{:feeder fzf.feeders.exec
		 :input "progs"
		 
		 :props {:nth {:delimiter " | "
					   :display ["1"]
					   :scope [".."]}}})
(local finder (fzf.show* settings))
(local result (fzf.capture finder))
(print result.out)




;;(local a (fzf.show fzf.feeders.cat "vals.txt" {:multi true }))
;;(print (. (fzf.capture a) :out))

(io.read)

